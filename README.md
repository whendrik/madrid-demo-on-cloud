# Madrid Housing Demo on IBM CP4D Cloud Starter Edition

![folium](https://gitlab.com/whendrik/madrid-demo-on-cp4d/-/raw/master/images/madrid_demo.gif)

*Updated for wml v4*

## Authentication the new `WML` Client

To authenticate, you need to create a [API Key](https://cloud.ibm.com/docs/account?topic=account-userapikey#userapikey) in your IBM Cloud Console. **API Keys** never expire, can be generated as much as you wish, are named, and can be deleted. With a **API Keys**, you can generate **IAM Tokens**. A **IAM Tokens** [expires after 1 hour](https://cloud.ibm.com/docs/account?topic=account-iamtoken_from_apikey), hence you are expected to freshly generate one with a **API Key** in each session. 

Watson Machine Learning (`WML`) needs a `IAM Token` to authenticate.

![folium](https://gitlab.com/whendrik/madrid-demo-on-cloud/-/raw/master/images/token.png)

1. Login on your [IBM CLoud](https://cloud.ibm.com/), `Manage` > `Access (IAM)` > `API keys`, and generate a new `API Key`.
2. With the `API Key` you can generate tokens manually, but it is most easy to let the `WML Client` generate a token, and authenticate with a single `wml_client = APIClient(wml_credentials)`. The notebook uses this method. 

https://dataplatform.cloud.ibm.com/docs/content/wsj/analyze-data/ml-authentication.html

**(!) Warning** - API Keys give full control over your Cloud Account. Alternatively use ServiceIDs.

## Cloud-wide API key & Service-IDs

It is not recommended to use your cloud wide `APIKEY` to access `wml`, alternatively, you can create a `ServiceID`;

1. Create a `ServiceID` (https://cloud.ibm.com/iam/serviceids)
2. Generate a `API key` for that `ServiceID`
3. Associate the `ServiceID` to a **Deployment Space** (on the Access control tab > Editor.
4. For scoring through the REST API, use the `ServiceID`'s API key in the same way as you'd use the IAM Key, i.e. get a token from 'https://iam.cloud.ibm.com/identity/token' and then use that for the scoring request
5. To use the python `wml` `SDK`; `Access policies` > `Assign Access` and add `Machine Learning`, and give it access to;
	- Viewer
	- Operator
	- Editor
	- Administrator (Needed for scoring with WML Python SDK)
	- (Service Access) Writer

#### Store and Deploy a Model

- See [examples](https://dataplatform.cloud.ibm.com/docs/content/wsj/analyze-data/ml-samples-overview.html) to check how to store and deploy models with the new API.

The notebook is based on the example above.

#### Scoring using a Online Deployment

- For API calls to a online model, a `version` param must be provided, this is shown in the example call below.

When a model is deployed, the `IAM Token` can be generated with `CURL` or `python requests`. [Examples](https://dataplatform.cloud.ibm.com/docs/content/wsj/analyze-data/ml-authentication.html) show the necessary python code, the example below is based on that.

#### `WML-`instances, Locations, and Deployment Spaces...

The different organisation mechanisms allow multiple WML instances to co-exist, all with different Deployment Spaces.

* A Watson Studio service is deployed in one of the cloud locations.
* A WML service is deployed in one of the [cloud locations](https://fra02.console.cloud.ibm.com/apidocs/machine-learning).
* Watson Studio & WML are best to be deployed in same location, but is not necessary. This makes management from the Web GUI easier.
* A [Deployment Space](https://dataplatform.cloud.ibm.com/docs/content/wsj/analyze-data/ml-spaces_local.html?audience=wdp) is associated to one [WML Service](https://dataplatform.cloud.ibm.com/docs/content/wsj/analyze-data/ml-overview.html). Therefore also linked to one location.
* From the Watson Studio Web GUI, you can only see the Deployment Spaces which are associated with same-location WML services.
* With the WML API, you authenticate by defining one location. When listing the Deployment Spaces from the API, you only see the deployment spaces from the same-location as selected when authenticating.

List of Location:

https://fra02.console.cloud.ibm.com/apidocs/machine-learning

### Example JSON for Call for Housing Demo

```
{"input_data": [{"fields": ["property_state", "distance_to_centre", "distance_to_metro", "mts2"], 
"values": [[1,7,1,120]]}]}
```

### Example Python snippet for calling the ONLINE Deployment

Alternative is to use the `wml` python client, like example in the [SciKit Learn Example](https://github.com/IBM/watson-machine-learning-samples/blob/master/notebooks/python_sdk/deployments/scikit-learn/Use%20scikit-learn%20to%20recognize%20hand-written%20digits.ipynb)
 

```
import json
import requests
import urllib3


apikey = "6S1YV............................NBe8B"


# Get an IAM token from IBM Cloud
url     = "https://iam.cloud.ibm.com/identity/token"
headers = { "Content-Type" : "application/x-www-form-urlencoded" }
data    = "apikey=" + apikey + "&grant_type=urn:ibm:params:oauth:grant-type:apikey"
IBM_cloud_IAM_uid = 'bx'
IBM_cloud_IAM_pwd = 'bx'
response  = requests.post( url, headers=headers, data=data, auth=( IBM_cloud_IAM_uid, IBM_cloud_IAM_pwd ) )
iam_token = response.json()["access_token"]

header = {'Content-Type': 'application/json', 'Authorization': 'Bearer ' + iam_token}

payload_scoring = {"input_data": [{"fields": ["property_state", "distance_to_centre", "distance_to_metro", "mts2"], 
"values": [[1,1,1,120]]}] }

response_scoring = requests.post('https://us-south.ml.cloud.ibm.com/ml/v4/deployments/cfc3385e-41e0-4df7-b2ad-fa5fe1a88ad4/predictions', json=payload_scoring, headers=header, params=[('version','2020-09-01')] )

json_response = response_scoring.json()

print(json_response)

```